package com.challenge.ecommerce.base

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.challenge.ecommerce.common.utils.WindowUtils.clearStatusBar
import com.challenge.ecommerce.common.utils.WindowUtils.setToolbarTopPadding
import com.challenge.ecommerce.common.utils.WindowUtils.setTransparentStatusBar

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTransparentStatusBar(this)
    }

    override fun setSupportActionBar(toolbar: Toolbar?) {
        super.setSupportActionBar(toolbar)

        toolbar?.let {
            setToolbarTopPadding(toolbar)
        }
    }

    fun clearStatusBar() {
        clearStatusBar(this)
    }
}