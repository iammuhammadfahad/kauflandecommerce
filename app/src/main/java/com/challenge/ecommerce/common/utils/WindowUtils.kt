package com.challenge.ecommerce.common.utils

import android.annotation.TargetApi
import android.app.Activity
import android.os.Build
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.updatePadding
import com.challenge.ecommerce.R

object WindowUtils {

    fun setTransparentStatusBar(activity: Activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            activity.window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
    }

    fun setToolbarTopPadding(toolbar: Toolbar) {
        toolbar.setOnApplyWindowInsetsListener { v, insets ->
            v.updatePadding(top = insets.systemWindowInsetTop)
            insets
        }
    }

    fun clearStatusBar(activity: Activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            setDarkStatusBar(activity, false)
        }

        var flags = activity.window.decorView.systemUiVisibility
        flags = flags and View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN.inv()
        activity.window.decorView.systemUiVisibility = flags
    }

    @TargetApi(23)
    private fun setDarkStatusBar(activity: Activity, colored: Boolean = true) {
        var flags = activity.window.decorView.systemUiVisibility
        flags = flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
        activity.window.decorView.systemUiVisibility = flags

        if (colored)
            activity.window.statusBarColor =
                ContextCompat.getColor(activity, R.color.colorStatusBarDark)
    }

}