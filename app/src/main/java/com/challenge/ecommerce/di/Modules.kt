package com.challenge.ecommerce.di

import com.challenge.ecommerce.data.repository.ProductsRemoteRepositoryImpl
import com.challenge.ecommerce.data.sources.remote.mapper.ProductsRemoteMapper
import com.challenge.ecommerce.domain.interactor.GetProductsUseCase
import com.challenge.ecommerce.domain.repository.ProductsRemoteRepository
import com.challenge.ecommerce.ui.home.master.ProductListAdapter
import com.challenge.ecommerce.ui.home.viewmodel.ProductViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    single { ProductsRemoteMapper() }
    factory<ProductsRemoteRepository> { ProductsRemoteRepositoryImpl(get()) }
    factory { ProductListAdapter(androidContext()) }
}

val ProductProductModule = module {
    factory { GetProductsUseCase(get()) }
    viewModel { ProductViewModel(get()) }
}