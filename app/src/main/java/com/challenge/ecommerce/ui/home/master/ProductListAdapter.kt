package com.challenge.ecommerce.ui.home.master

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.challenge.ecommerce.R
import com.challenge.ecommerce.common.glide.load
import com.challenge.ecommerce.common.utils.dp
import com.challenge.ecommerce.data.sources.remote.api.ApiClient
import com.challenge.ecommerce.databinding.RowItemListBinding
import com.challenge.ecommerce.model.ResultsItem
import java.util.*

class ProductListAdapter(val context: Context?, var items: List<ResultsItem> = ArrayList()) :
    RecyclerView.Adapter<ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(resultsItem: ResultsItem, container: View)
    }

    private var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            RowItemListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        holder.tvItemTitle.text = item.title

        val euro: String = Currency.getInstance(Locale.GERMANY).currencyCode
        val price: String? = item.price

        holder.tvItemDescription.text = "$euro $price"

        holder.ivItemPoster.setImageResource(R.drawable.poster_placeholder)
        holder.ivItemPoster.transitionName = item.id.toString()
        holder.llItemTextContainer.setBackgroundColor(Color.DKGRAY)

        val url = item.picture
        if (url != null) {
            val itemImage = url[0]
            val picUrl = "$itemImage.jpg"
            holder.ivItemPoster.load(
                url = ApiClient.POSTER_BASE_URL + picUrl,
                crossFade = true, width = 160.dp, height = 160.dp
            ) { color ->
                holder.llItemTextContainer.setBackgroundColor(color)
            }
        }

        holder.cvItemContainer.setOnClickListener {
            onItemClickListener?.onItemClick(item, holder.ivItemPoster)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setOnProductClickListener(onItemClickListener: OnItemClickListener) {
        this.onItemClickListener = onItemClickListener
    }

    fun fillList(items: List<ResultsItem>) {
        this.items += items
        notifyDataSetChanged()
    }

    fun clear() {
        this.items = emptyList()
    }
}

class ViewHolder(binding: RowItemListBinding) : RecyclerView.ViewHolder(binding.root) {
    val cvItemContainer: CardView = binding.cvItemContainer
    val llItemTextContainer: LinearLayout = binding.llTextContainer
    val tvItemTitle: TextView = binding.tvItemTitle
    val tvItemDescription: TextView = binding.tvItemDescription
    val ivItemPoster: ImageView = binding.ivItemPoster
}