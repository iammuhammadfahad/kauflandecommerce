package com.challenge.ecommerce.ui.details

import android.os.Bundle
import androidx.navigation.ActivityNavigator
import com.challenge.ecommerce.base.BaseActivity
import com.challenge.ecommerce.common.glide.load
import com.challenge.ecommerce.common.utils.ColorUtils.darken
import com.challenge.ecommerce.common.utils.dp
import com.challenge.ecommerce.common.utils.gone
import com.challenge.ecommerce.data.sources.remote.api.ApiClient
import com.challenge.ecommerce.databinding.ActivityItemDetailsBinding
import com.challenge.ecommerce.model.ResultsItem
import com.google.gson.Gson

class ProductDetailsActivity : BaseActivity() {

    private lateinit var binding: ActivityItemDetailsBinding
    private lateinit var item: ResultsItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityItemDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        item =
            Gson().fromJson(intent?.extras?.getString("ItemData"), ResultsItem::class.java)

        setupToolbar()
        clearStatusBar()
        setupPosterImage()
        setupDetails()
    }

    private fun setupDetails() {
        postponeEnterTransition()
        binding.ivActivityProductDetails.transitionName = item.id.toString()

        binding.progressBar.gone()

        binding.detailDescription.text = item.description

        if (item.price != null) {
            binding.detailExtraInfo.detailPrice.text = item.price
        }

        if (item.discount != null) {
            binding.detailExtraInfo.detailDiscount.text = item.discount.toString()
        }
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        binding.collapsingToolbar.title = item.title

    }

    private fun setupPosterImage() {
        postponeEnterTransition()

        binding.ivActivityProductDetails.transitionName = item.id.toString()

        val url = item.picture
        if (url != null) {
            val itemImage = url[0]
            val picUrl = "$itemImage.jpg"
            binding.ivActivityProductDetails.load(
                url = ApiClient.POSTER_BASE_URL + picUrl,
                width = 160.dp,
                height = 160.dp
            ) { color ->
                window?.statusBarColor = color.darken
                binding.collapsingToolbar.setBackgroundColor(color)
                binding.collapsingToolbar.setContentScrimColor(color)
                startPostponedEnterTransition()
            }
        }
    }
}