package com.challenge.ecommerce.ui.home.viewmodel

import androidx.lifecycle.ViewModel
import com.challenge.ecommerce.data.Resource
import com.challenge.ecommerce.domain.interactor.GetProductsUseCase
import com.challenge.ecommerce.model.ProductsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class ProductViewModel(private val getProductsUseCase: GetProductsUseCase) :
    ViewModel() {

    private val stateFlow = MutableStateFlow<Resource<ProductsResponse>>(Resource.empty())
    private var currentPage = 1
    private var lastPage = 1

    var disposable: Disposable? = null

    val trendingProductState: StateFlow<Resource<ProductsResponse>>
        get() = stateFlow

    private fun fetchTrendingProduct() {
        stateFlow.value = Resource.loading()

        disposable = getProductsUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ res ->
                stateFlow.value = Resource.success(res)
            }, { throwable ->
                lastPage = currentPage
                throwable.localizedMessage?.let {
                    stateFlow.value = Resource.error(it)
                }
            })
    }

    fun fetchNextTrendingProducts() {
        currentPage++
        fetchTrendingProduct()
    }

    fun refreshProducts() {
        currentPage = 1
        fetchTrendingProduct()
    }

    fun isFirstPage(): Boolean {
        return currentPage == 1
    }

    fun isLastPage(): Boolean {
        return currentPage == lastPage
    }

}