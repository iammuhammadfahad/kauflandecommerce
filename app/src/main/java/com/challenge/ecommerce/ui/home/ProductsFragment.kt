package com.challenge.ecommerce.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.challenge.ecommerce.R
import com.challenge.ecommerce.common.recyclerview.PaginationScrollListener
import com.challenge.ecommerce.common.utils.gone
import com.challenge.ecommerce.common.utils.visible
import com.challenge.ecommerce.data.Resource
import com.challenge.ecommerce.databinding.FragmentProductListBinding
import com.challenge.ecommerce.model.ProductsResponse
import com.challenge.ecommerce.model.ResultsItem
import com.challenge.ecommerce.ui.details.ProductDetailsActivity
import com.challenge.ecommerce.ui.home.master.ProductListAdapter
import com.challenge.ecommerce.ui.home.viewmodel.ProductViewModel
import com.google.gson.Gson
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber

class ProductsFragment : Fragment(R.layout.fragment_product_list),
    ProductListAdapter.OnItemClickListener {

    private val productViewModel: ProductViewModel by sharedViewModel()
    private val productListAdapter: ProductListAdapter by inject()

    private var _binding: FragmentProductListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupRecyclerView()
        setupSwipeRefresh()

        productViewModel.refreshProducts()

        viewLifecycleOwner.lifecycleScope.launch {
            productViewModel.trendingProductState.collect {
                handleProductsDataState(it)
            }
        }
    }

    override fun onItemClick(resultsItem: ResultsItem, container: View) {
        val intent = Intent(this.context, ProductDetailsActivity::class.java)
        intent.putExtra("ItemData", Gson().toJson(resultsItem))
        startActivity(intent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

        productViewModel.disposable?.dispose()
    }

    private fun handleProductsDataState(state: Resource<ProductsResponse>) {
        when (state.status) {
            Resource.Status.LOADING -> {
                binding.srlFragmentProductList.isRefreshing = true
            }
            Resource.Status.SUCCESS -> {
                binding.srlFragmentProductList.isRefreshing = false
                state.data?.let { loadProducts(it) }
            }
            Resource.Status.ERROR -> {
                binding.srlFragmentProductList.isRefreshing = false
                binding.pbFragmentProductList.gone()
            }
            Resource.Status.EMPTY -> {
                Timber.d("Empty state.")
            }
        }
    }

    private fun loadProducts(products: ProductsResponse) {
        products.let {
            if (productViewModel.isFirstPage()) {
                productListAdapter.clear()
            }

            productListAdapter.fillList(it.results)
        }
    }

    private fun setupRecyclerView() {
        productListAdapter.setOnProductClickListener(this)

        binding.rvFragmentProductList.adapter = productListAdapter
        binding.rvFragmentProductList.addOnScrollListener(object :
            PaginationScrollListener(binding.rvFragmentProductList.linearLayoutManager) {
            override fun isLoading(): Boolean {
                val isLoading = binding.srlFragmentProductList.isRefreshing

                if (isLoading) {
                    binding.pbFragmentProductList.visible()
                } else {
                    binding.pbFragmentProductList.gone()
                }

                return isLoading
            }

            override fun isLastPage(): Boolean {
                return productViewModel.isLastPage()
            }

            override fun loadMoreItems() {
                productViewModel.fetchNextTrendingProducts()
            }
        })
    }

    private fun setupSwipeRefresh() {
        binding.srlFragmentProductList.setOnRefreshListener {
            productViewModel.refreshProducts()
        }
    }
}