package com.challenge.ecommerce.ui.home

import android.os.Bundle
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.challenge.ecommerce.R
import com.challenge.ecommerce.base.BaseActivity
import com.challenge.ecommerce.databinding.ActivityHomeBinding

class HomeActivity : BaseActivity() {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<ProductsFragment>(R.id.nav_host_container)
            }
        }
    }
}