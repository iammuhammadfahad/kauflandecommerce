package com.challenge.ecommerce.model

data class ProductsResponse(

    val results: List<ResultsItem>
)

data class ResultsItem(

    val price: String? = null,

    val description: String? = null,

    val discount: Any? = null,

    val id: Int? = null,

    val title: String? = null,

    val picture: List<String?>? = null,

    val energy: Any? = null
)

data class Energy(

    val spectrum: String? = null,

    val jsonMemberClass: String? = null,

    val efficiencyNr: Int? = null
)
