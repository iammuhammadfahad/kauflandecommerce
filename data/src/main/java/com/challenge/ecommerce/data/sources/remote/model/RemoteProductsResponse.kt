package com.challenge.ecommerce.data.sources.remote.model

import com.google.gson.annotations.SerializedName

data class RemoteProductsResponse(

    @field:SerializedName("results")
    val results: List<ResultsItem>
)

data class ResultsItem(

    @field:SerializedName("price")
    val price: String,

    @field:SerializedName("description")
    val description: String,

    @field:SerializedName("discount")
    val discount: Any,

    @field:SerializedName("id")
    val id: Int,

    @field:SerializedName("title")
    val title: String,

    @field:SerializedName("picture")
    val picture: List<String>,

    @field:SerializedName("energy")
    val energy: Energy
)

data class Energy(

    @field:SerializedName("spectrum")
    val spectrum: String,

    @field:SerializedName("class")
    val jsonMemberClass: String,

    @field:SerializedName("efficiencyNr")
    val efficiencyNr: Int
)
