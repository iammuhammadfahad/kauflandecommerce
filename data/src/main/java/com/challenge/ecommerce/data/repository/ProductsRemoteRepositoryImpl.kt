package com.challenge.ecommerce.data.repository

import com.challenge.ecommerce.data.sources.remote.api.ApiClient
import com.challenge.ecommerce.data.sources.remote.mapper.ProductsRemoteMapper
import com.challenge.ecommerce.domain.repository.ProductsRemoteRepository
import com.challenge.ecommerce.model.ProductsResponse
import io.reactivex.Single

class ProductsRemoteRepositoryImpl(private val productsRemoteMapper: ProductsRemoteMapper) : ProductsRemoteRepository  {

    override fun getProducts(): Single<ProductsResponse> {
        return ApiClient.productsService().getProducts().map {
            productsRemoteMapper.mapFromRemote(it)
        }
    }
}