package com.challenge.ecommerce.data.sources.remote.service

import com.challenge.ecommerce.data.sources.remote.model.RemoteProductsResponse
import io.reactivex.Single
import retrofit2.http.GET

interface ProductService {

    @GET("products")
    fun getProducts(): Single<RemoteProductsResponse>

}