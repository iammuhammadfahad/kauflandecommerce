package com.challenge.ecommerce.data.sources.remote.mapper

import com.challenge.ecommerce.data.sources.remote.model.RemoteProductsResponse
import com.challenge.ecommerce.model.ProductsResponse
import com.challenge.ecommerce.model.ResultsItem

class ProductsRemoteMapper {

    fun mapFromRemote(response: RemoteProductsResponse): ProductsResponse {
        return ProductsResponse(response.results.map {
            ResultsItem(
                it.price,
                it.description,
                it.discount,
                it.id,
                it.title,
                it.picture,
                it.energy
            )
        })
    }
}