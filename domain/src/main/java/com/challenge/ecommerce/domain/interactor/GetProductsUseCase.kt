package com.challenge.ecommerce.domain.interactor

import com.challenge.ecommerce.domain.repository.ProductsRemoteRepository
import com.challenge.ecommerce.model.ProductsResponse
import io.reactivex.Single

class GetProductsUseCase(private val productsRemoteRepository: ProductsRemoteRepository) {

    fun execute(): Single<ProductsResponse> {
        return productsRemoteRepository.getProducts()
    }
}