package com.challenge.ecommerce.domain.repository

import com.challenge.ecommerce.model.ProductsResponse
import io.reactivex.Single

interface ProductsRemoteRepository {

    fun getProducts(): Single<ProductsResponse>

}